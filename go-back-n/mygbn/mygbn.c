#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <time.h>
#include <unistd.h>
#include <fcntl.h>

#include "mygbn.h"

int sendFunctionCallCount = 0;
bool mainThreadBlocked = false;

void *receive_acknowledgement(void *GBNsender){
  // TODO: Read from socket and receive acknowledgement packets
  //       mutex_lock when updating acknowledgement sequence number
  printf("(Thread 1) Start acknoledgement thread\n");
  struct mygbn_sender *sender = ((struct mygbn_sender *) GBNsender);
  struct MYGBN_Packet receiveFromServer;


  while(!(sender->termination)){
    int ret = recvfrom(sender->sd, (void *)&receiveFromServer, sizeof(receiveFromServer), 0, NULL, NULL);
    if(ret == 0)
      break;

    if(strncmp((char *) receiveFromServer.protocol, "gbn", 3) != 0)
      continue;
  
    receiveFromServer.seqNum = ntohl(receiveFromServer.seqNum);
    receiveFromServer.length = ntohl(receiveFromServer.length);

    // It should be next packet in sequential order
    printf("(Thread 1) Receive From Server with sequence: %d\n", receiveFromServer.seqNum);

    if(receiveFromServer.type == 0xA1){
      // Receive Ack Packet
      if(receiveFromServer.seqNum > sender->ackNumber){
        while(receiveFromServer.seqNum > sender->ackNumber)
          dequeue_sendPacket(sender);
        printf("(Thread 1) Used window size: %d\n", sender->usedWindowSize);
      }
      else
        printf("(Thread 1) Wrong Ack Sequence: Expected: %d, Received: %d\n", sender->ackNumber + 1, receiveFromServer.seqNum);
    }
    else
      printf("(Thread 1) Wrong type: %d\n", receiveFromServer.type);
  }

  pthread_exit(NULL);
}

void enqueue_sendPacket(struct mygbn_sender* mygbn_sender, struct MYGBN_Packet *packetPtr){
    // Called when data is sent and sequence number is increased to latest packet sent
    time_t current_time;
    time(&current_time);
    pthread_mutex_lock(&(mygbn_sender->sequenceLock));
    printf("Sequence Number: %d\n", mygbn_sender->sequenceNumber);
    printf("Acknowledgement Number: %d\n", mygbn_sender->ackNumber);
    printf("Enqueue to index %d\n", mygbn_sender->sequenceNumber - mygbn_sender->ackNumber - 1);
    mygbn_sender->sendTime[mygbn_sender->sequenceNumber - mygbn_sender->ackNumber - 1] = current_time;
    mygbn_sender->sentPacket[mygbn_sender->sequenceNumber - mygbn_sender->ackNumber - 1] = (struct MYGBN_Packet *) malloc(sizeof(struct MYGBN_Packet));
    memcpy(mygbn_sender->sentPacket[mygbn_sender->sequenceNumber - mygbn_sender->ackNumber - 1], packetPtr, sizeof(struct MYGBN_Packet));
    mygbn_sender->usedWindowSize += 1;
    pthread_mutex_unlock(&(mygbn_sender->sequenceLock));
}

void dequeue_sendPacket(struct mygbn_sender* mygbn_sender){
    // Called when acknowledgement received and ack number increased by 1
    pthread_mutex_lock(&(mygbn_sender->sequenceLock));
    free(mygbn_sender->sentPacket[0]);
    for(int i=1; i<mygbn_sender->usedWindowSize; i++){
      mygbn_sender->sendTime[i - 1] = mygbn_sender->sendTime[i];
      mygbn_sender->sentPacket[i - 1] = mygbn_sender->sentPacket[i];
    }
    mygbn_sender->usedWindowSize -= 1;
    mygbn_sender->ackNumber += 1;
    pthread_mutex_unlock(&(mygbn_sender->sequenceLock));
}

void *timeout_thread(void *GBNsender){
  // TODO: Keep counting oldest un-acked packet
  //       Block mygbn_send when windows is fulled and send again all packets in windws
  printf("(Thread 3) timeout_thread started\n");
  struct mygbn_sender *sender = ((struct mygbn_sender *) GBNsender);
  time_t current_time;
  while(!(sender->termination)){
    pthread_mutex_lock(&(sender->timeoutLock));
    // printf("(Thread 3) timeout_thread locked\n");

    if(time(&current_time) - sender->sendTime[0] >= sender->timeout){
      printf("(Thread 3) Timeout and re-send all packets in window\n");
      pthread_mutex_lock(&(sender->sequenceLock));
      // Re-send all packets
      printf("(Thread 3) Current Sequence Number: %d\n", sender->sequenceNumber);
      printf("(Thread 3) Current Acknowledgement Number: %d\n", sender->ackNumber);
      for(int i=0; i<sender->usedWindowSize; i++){
        printf("(Thread 3) Re-send Sequence Number: %d\n", sender->ackNumber + 1 + i);
        // struct MYGBN_Packet *packet = packet_generator(0xA0, sender->ackNumber + 1 + i, sender->sentPacket[i], strlen((char *) sender->sentPacket[i]));
        int packetLength = ntohl(sender->sentPacket[i]->length);
        sendto(sender->sd, (void *)sender->sentPacket[i], packetLength, 0, (struct sockaddr *) &(sender->destination), sizeof(sender->destination));
        sender->sendTime[i] = time(&current_time);
      }
      pthread_mutex_unlock(&(sender->sequenceLock));
    }

    // printf("(Thread 3) timeout_thread wake up main thread\n");
    pthread_cond_signal(&(sender->cond_wait));

    pthread_mutex_unlock(&(sender->timeoutLock));
    // printf("(Thread 3) timeout_thread unlocked\n");
  }
  pthread_mutex_unlock(&(sender->timeoutLock));
  pthread_exit(NULL);
}

void mygbn_init_sender(struct mygbn_sender* mygbn_sender, char* ip, int port, int N, int timeout){
  // create a IPv4/UDP socket
  mygbn_sender->sd = socket(AF_INET, SOCK_DGRAM, 0);

  // initialize the address
  memset(&mygbn_sender->destination, 0, sizeof(struct sockaddr_in));
  mygbn_sender->destination.sin_family = AF_INET;
  inet_pton(AF_INET, ip, &(mygbn_sender->destination.sin_addr));
  mygbn_sender->destination.sin_port = htons(port);

  mygbn_sender->termination = false;

  pthread_cond_init(&(mygbn_sender->cond_wait), NULL);
  pthread_mutex_init(&(mygbn_sender->timeoutLock), NULL);
  pthread_mutex_init(&(mygbn_sender->sequenceLock), NULL);

  mygbn_sender->windowSize = N;
  mygbn_sender->usedWindowSize = 0;
  mygbn_sender->timeout = timeout;

  mygbn_sender->sequenceNumber = 1;
  mygbn_sender->ackNumber = 0;

  mygbn_sender->sendTime = (time_t *) malloc(sizeof(time_t) * mygbn_sender->windowSize);
  mygbn_sender->sentPacket = (struct MYGBN_Packet **) malloc(sizeof(struct MYGBN_Packet *) * mygbn_sender->windowSize);

  pthread_create(&(mygbn_sender->acknowledgeThread), NULL, receive_acknowledgement, mygbn_sender);

  pthread_mutex_lock(&(mygbn_sender->timeoutLock));

  pthread_create(&(mygbn_sender->timeoutThread), NULL, timeout_thread, mygbn_sender);
  printf("(Thread 0) Finish sender initialization\n");

  sleep(2);
}

int mygbn_send(struct mygbn_sender* mygbn_sender, unsigned char* buf, int len){
  // TODO: Split all sending files into size <= 512 bytes
  //       cond_wait if windows size is fulled and go to timeout_thread
  //       Every sent packets will be marked in windows and with its own send time
  printf("(Thread 0) Start sending string\n");
  sendFunctionCallCount += 1;
  time_t current_time;

  if(mygbn_sender->usedWindowSize > 0){
    if(time(&current_time) - mygbn_sender->sendTime[0] > mygbn_sender->timeout){
      printf("(Thread 0) Timeout and pass to timeout_thread\n");
      pthread_cond_wait(&(mygbn_sender->cond_wait), &(mygbn_sender->timeoutLock));
    }
  }

  if(len < MAX_PAYLOAD_SIZE){
    // No timeout is allowed and main thread acquired itself
    // pthread_mutex_lock(&(mygbn_sender->timeoutLock));
    // printf("Mutex lock not to let other thread lock main thread\n");
    while(mygbn_sender->usedWindowSize == mygbn_sender->windowSize){
      // printf("(Thread 0) Window is full and wait for shifting and call timeout_thread\n");
      pthread_cond_wait(&mygbn_sender->cond_wait, &mygbn_sender->timeoutLock);
    }

    struct MYGBN_Packet *packet = packet_generator(0xA0, mygbn_sender->sequenceNumber, buf, len);
    int packetLength = ntohl(packet->length);
    enqueue_sendPacket(mygbn_sender, packet);
    sendto(mygbn_sender->sd, (void *)packet, packetLength, 0, (struct sockaddr *) &(mygbn_sender->destination), sizeof(mygbn_sender->destination));
    printf("(Thread 0) Send string with sequence %d.\n", mygbn_sender->sequenceNumber);
    free(packet);
    mygbn_sender->sequenceNumber += 1;

    printf("(Thread 0) Main thread wake up ack thread\n");
  }
  else{
    int partitionCount = 0;
    while(len > 0){
      // Partition string to less than or equal to 512 bytes
      unsigned char partitionString[MAX_PAYLOAD_SIZE];
      int partitionLength = (len > MAX_PAYLOAD_SIZE) ? MAX_PAYLOAD_SIZE : len;
      memcpy(partitionString, &buf[partitionCount], partitionLength);
      //

      // No timeout is allowed and main thread acquired itself
      // pthread_mutex_lock(&(mygbn_sender->timeoutLock));
      // printf("Mutex lock not to let other thread lock main thread\n");
      while(mygbn_sender->usedWindowSize == mygbn_sender->windowSize){
        // printf("(Thread 0) Window is full and wait for shifting and call timeout_thread\n");
        pthread_cond_wait(&mygbn_sender->cond_wait, &mygbn_sender->timeoutLock);
      }

      // Create packet and send to server
      struct MYGBN_Packet *packet = packet_generator(0xA0, mygbn_sender->sequenceNumber, partitionString, partitionLength);
      int packetLength = ntohl(packet->length);
      enqueue_sendPacket(mygbn_sender, packet);
      sendto(mygbn_sender->sd, (void *)packet, packetLength, 0, (struct sockaddr *) &(mygbn_sender->destination), sizeof(mygbn_sender->destination));
      printf("(Thread 0) Send string with sequence %d.\n", mygbn_sender->sequenceNumber);
      mygbn_sender->sequenceNumber += 1;
      //
      
      len -= MAX_PAYLOAD_SIZE;
      partitionCount += partitionLength;
      free(packet);

      printf("(Thread 0) Main thread wake up ack thread\n");
      sleep(1);
    }
  }
  sleep(1);
  printf("(Thread 0) End sending string\n");
  return 0;
}

void mygbn_close_sender(struct mygbn_sender* mygbn_sender){
  // TODO: Wait until all packets are received by client i.e. sequence number = acknowledgement number
  //       Send an End packet to client and wait for acknowledgement
  time_t current_time;
  int end_packet_retransmit = 0;
  // Ensure all data packets had been received by receiver side
  while(mygbn_sender->usedWindowSize > 0){
    if(time(&current_time) - mygbn_sender->sendTime[0] > mygbn_sender->timeout){
      printf("(Thread 0) Timeout and pass to timeout_thread\n");
      pthread_cond_wait(&(mygbn_sender->cond_wait), &(mygbn_sender->timeoutLock));
    }
    sleep(1);
  }

  struct MYGBN_Packet *packet = packet_generator(0xA2, mygbn_sender->sequenceNumber, NULL, 0);
  int packetLength = ntohl(packet->length);
  enqueue_sendPacket(mygbn_sender, packet);
  sendto(mygbn_sender->sd, (void *)packet, packetLength, 0, (struct sockaddr *) &(mygbn_sender->destination), sizeof(mygbn_sender->destination));
  printf("(Thread 0) Send string with sequence %d.\n", mygbn_sender->sequenceNumber);
  free(packet);
  mygbn_sender->sequenceNumber += 1;

  while(mygbn_sender->usedWindowSize > 0){
    if(time(&current_time) - mygbn_sender->sendTime[0] > mygbn_sender->timeout){
      end_packet_retransmit += 1;
      printf("(Thread 0) Timeout and pass to timeout_thread\n");
      pthread_cond_wait(&(mygbn_sender->cond_wait), &(mygbn_sender->timeoutLock));
    }
    if(end_packet_retransmit >= 3){
      printf("(Error) Server does not reply end packet acknowledgement.\n");
      break;
    }
    sleep(1);
  }

  shutdown(mygbn_sender->sd, SHUT_RDWR);
  close(mygbn_sender->sd);
  // Main thread can be locked by timer or whatever
  pthread_mutex_unlock(&(mygbn_sender->timeoutLock));
  mygbn_sender->termination = true;
  pthread_cond_wait(&(mygbn_sender->cond_wait), &(mygbn_sender->timeoutLock));
  pthread_join(mygbn_sender->acknowledgeThread, NULL);
  pthread_join(mygbn_sender->timeoutThread, NULL);

  return;
}

void mygbn_init_receiver(struct mygbn_receiver* mygbn_receiver, int port){
  // create a IPv4/UDP socket
  mygbn_receiver->sd = socket(AF_INET, SOCK_DGRAM, 0);

  // initialize the address
  memset(&mygbn_receiver->address, 0, sizeof(struct sockaddr_in));
  mygbn_receiver->address.sin_family = AF_INET;
  mygbn_receiver->address.sin_port = htons(port);
  mygbn_receiver->address.sin_addr.s_addr = htonl(INADDR_ANY); 

  // Bind the socket to the address
  bind(mygbn_receiver->sd, (struct sockaddr *) &mygbn_receiver->address, sizeof(struct sockaddr));

  // Need to reset when a sender request is done
  mygbn_receiver->expectedSequenceNumber = 1;

  mygbn_receiver->startfile = false;
  printf("(Thread 0) Finish receiver initialization\n");
}

int mygbn_recv(struct mygbn_receiver* mygbn_receiver, unsigned char* buf, int len){
  // TODO: Receive packets and check for contents
  //       If packet sequence number = acknowledgement+1 number then send ack+1 packet
  //       Else simply ignore that packet
  printf("(Thread 0) Start receiving string\n");

  int addressSize = sizeof(mygbn_receiver->address);
  bool correctPacket = false;

  while(!correctPacket){
    struct MYGBN_Packet receiveFromClient;
    recvfrom(mygbn_receiver->sd, (void *)&receiveFromClient, sizeof(receiveFromClient), 0, (struct sockaddr *) &(mygbn_receiver->address), (socklen_t *) &addressSize);

    // Only gbn protocol message will be processed
    if(strncmp((char *) receiveFromClient.protocol, "gbn", 3) != 0)
      continue;
    
    receiveFromClient.seqNum = ntohl(receiveFromClient.seqNum);
    receiveFromClient.length = ntohl(receiveFromClient.length);

    // It should be next packet in sequential order
    printf("(Thread 0) Receive From Client with sequence: %d\n", receiveFromClient.seqNum);

    if(receiveFromClient.type == 0xA0){
      if(receiveFromClient.seqNum != mygbn_receiver->expectedSequenceNumber){
        // Some packets are lost
        struct MYGBN_Packet *packet = packet_generator(0xA1, mygbn_receiver->expectedSequenceNumber - 1, NULL, 0);
        int packetLength = ntohl(packet->length);
        sendto(mygbn_receiver->sd, (void *)packet, packetLength, 0, (struct sockaddr *) &(mygbn_receiver->address), sizeof(mygbn_receiver->address));
        free(packet);
        continue;
      }
      // Data Packet
      int datalength = receiveFromClient.length
                      - sizeof(receiveFromClient.protocol)
                      - sizeof(receiveFromClient.length)
                      - sizeof(receiveFromClient.type)
                      - sizeof(receiveFromClient.seqNum);
      if(datalength == 0)
        continue;
      mygbn_receiver->startfile = true;
      printf("(Thread 0) Receive Data Length: %d\n", datalength);
      memcpy(buf, receiveFromClient.payload, datalength);
      struct MYGBN_Packet *packet = packet_generator(0xA1, mygbn_receiver->expectedSequenceNumber, NULL, 0);
      int packetLength = ntohl(packet->length);
      sendto(mygbn_receiver->sd, (void *)packet, packetLength, 0, (struct sockaddr *) &(mygbn_receiver->address), sizeof(mygbn_receiver->address));
      free(packet);
      mygbn_receiver->expectedSequenceNumber += 1;
      correctPacket = true;
      return datalength;
    }
    else if(receiveFromClient.type == 0xA1){
      // Ack Packet
      // Suppose there will not be any Ack Packet received in client side
      printf("(Thread 0) Ack Packet is received but it should not\n");
    }
    else if(receiveFromClient.type == 0xA2){
      if(receiveFromClient.seqNum != mygbn_receiver->expectedSequenceNumber && mygbn_receiver->startfile == true){
        // Some packets are lost
        struct MYGBN_Packet *packet = packet_generator(0xA1, mygbn_receiver->expectedSequenceNumber - 1, NULL, 0);
        int packetLength = ntohl(packet->length);
        sendto(mygbn_receiver->sd, (void *)packet, packetLength, 0, (struct sockaddr *) &(mygbn_receiver->address), sizeof(mygbn_receiver->address));
        free(packet);
        continue;
      }
      // End Packet
      printf("(Thread 0) End Packet is received\n");
      struct MYGBN_Packet *packet = packet_generator(0xA1, mygbn_receiver->expectedSequenceNumber, NULL, 0);
      int packetLength = ntohl(packet->length);
      sendto(mygbn_receiver->sd, (void *)packet, packetLength, 0, (struct sockaddr *) &(mygbn_receiver->address), sizeof(mygbn_receiver->address));
      free(packet);
      mygbn_receiver->expectedSequenceNumber = 1;
      mygbn_receiver->startfile = false;
      printf("\n\n(Thread 0) Expected Sequence Number: %d\n", mygbn_receiver->expectedSequenceNumber);
    }
  }

  printf("(Thread 0) End receiving string\n");
  return 0;
}

void mygbn_close_receiver(struct mygbn_receiver* mygbn_receiver) {
  // TODO: Receive End packet then acknowledge sender
  //       Reset all variables in mygbn_init_receiver function
  close(mygbn_receiver->sd);
}

struct MYGBN_Packet * packet_generator(unsigned char type, unsigned int sequenceNumber, unsigned char* payload, int length){
  // remember to free the heap space after sending this packet
  struct MYGBN_Packet *packet = (struct MYGBN_Packet *) malloc(sizeof(struct MYGBN_Packet));
  // strcpy(packet->protocol, "gbn");
  memcpy(packet->protocol, "gbn", strlen("gbn"));
  packet->seqNum = htonl(sequenceNumber);
  packet->type = type;
  memcpy(packet->payload, payload, length);
  packet->length = sizeof(packet->length) + sizeof(packet->protocol) + sizeof(packet->type) + sizeof(packet->seqNum) + length;
  packet->length = htonl(packet->length);
  return packet;
}