/*
 * mygbn.h
 */

// Reference: https://en.wikipedia.org/wiki/Go-Back-N_ARQ
// Pseudo-Code of Go-Back-N

#ifndef __mygbn_h__
#define __mygbn_h__

#include <stdbool.h>

#define MAX_PAYLOAD_SIZE 512

struct MYGBN_Packet {
  unsigned char protocol[3];                  /* protocol string (3 bytes) "gbn" */
  unsigned char type;                         /* type (1 byte) */
  unsigned int seqNum;                        /* sequence number (4 bytes) */
  unsigned int length;                        /* length(header+payload) (4 bytes) */
  unsigned char payload[MAX_PAYLOAD_SIZE];    /* payload data */
} mygbn_packet;

struct mygbn_sender {
  int sd; // GBN sender socket
  struct sockaddr_in destination;
  unsigned int sequenceNumber;
  unsigned int ackNumber;
  int timeout;

  int windowSize;
  int usedWindowSize;

  time_t *sendTime;
  struct MYGBN_Packet **sentPacket;

  pthread_cond_t cond_wait;
  pthread_mutex_t timeoutLock;

  pthread_mutex_t sequenceLock;

  pthread_t acknowledgeThread;
  pthread_t timeoutThread;

  bool termination;
};

void *receive_acknowledgement(void *GBNsender);
void enqueue_sendPacket(struct mygbn_sender* mygbn_sender, struct MYGBN_Packet *packetPtr);
void dequeue_sendPacket(struct mygbn_sender* mygbn_sender);

void mygbn_init_sender(struct mygbn_sender* mygbn_sender, char* ip, int port, int N, int timeout);
int mygbn_send(struct mygbn_sender* mygbn_sender, unsigned char* buf, int len);
void mygbn_close_sender(struct mygbn_sender* mygbn_sender);

struct mygbn_receiver {
  int sd; // GBN receiver socket
  struct sockaddr_in address;
  unsigned int expectedSequenceNumber;
  bool startfile;
};

void mygbn_init_receiver(struct mygbn_receiver* mygbn_receiver, int port);
int mygbn_recv(struct mygbn_receiver* mygbn_receiver, unsigned char* buf, int len);
void mygbn_close_receiver(struct mygbn_receiver* mygbn_receiver);

struct MYGBN_Packet * packet_generator(unsigned char type, unsigned int sequenceNumber, unsigned char* payload, int length);

#endif
