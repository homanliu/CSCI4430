#!/bin/sh

iptables -t filter -F
iptables -A INPUT -p udp -s 127.0.0.1 -d 127.0.0.1 -j NFQUEUE --queue-num 0
iptables -A OUTPUT -p udp -s 127.0.0.1 -d 127.0.0.1 -j NFQUEUE --queue-num 0
./troller 0.2 0.4 &
trollerPID=$!

make

./myftpserver 7777 &
serverPID=$!

fileSize=("100" "500" "1K" "5K" "8K" "10K" "50K" "80K" "100K")

for (( i=1; i <= ${#fileSize[@]}; ++i )); do
    generateFile="testcase"$i".txt"
    # fallocate -l $generateFile
    dd if=/dev/urandom of=$generateFile bs=${fileSize[i]} count=10
    ./myftpclient 127.0.0.1 7777 $generateFile 5 10
    originalFile=`md5sum ${generateFile} | awk '{ print $1 }'`
    receivedFile=`md5sum data/${generateFile} | awk '{ print $1 }'`

    if [ $originalFile != $receivedFile ]; then
        echo "MD5sum mismatch for ftp put testcase "$i
        exit 1
    else
        echo "MD5sum okay for ftp put testcase "$i
    fi

    rm $generateFile data/$generateFile
done

kill $serverPID
kill $trollerPID
iptables -t filter -F