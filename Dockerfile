FROM ubuntu:latest
MAINTAINER Liu Ho Man "hmliu6@gmail.com"

RUN apt-get upgrade -y \
    && apt-get update -y

RUN apt-get install -y vim \
    && apt-get install -y git \
    && apt-get install -y make \
    && apt-get install -y tcpdump \
    && apt-get install -y net-tools \
    && apt-get install -y build-essential \
    && apt-get install -y libnetfilter-queue-dev \
    && apt-get install -y iptables

RUN apt-get install -y tcpick