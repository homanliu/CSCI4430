#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <math.h>
#include <inttypes.h>
#include <string.h>
#include <assert.h>
#include <pthread.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <linux/types.h>
#include <linux/netfilter.h>
#include <libnetfilter_queue/libnetfilter_queue.h>
#include <libnfnetlink/libnfnetlink.h>
#include "checksum.h"

#define packet_buffer_size 10
#define translation_port_size 2001

struct nfq_handle *nfqueue_handler;
struct nfq_q_handle *queue_handler;
int nfqueue_fd;

char *nat_ip, *local_network;
int subnet_mask, bucket_size, fill_rate;
uint32_t LAN_number;

struct address_tuple{
    char ip_address[16];
    uint16_t port_number;
};

struct nat_mapping{
    struct address_tuple *source_address;
    struct address_tuple *translated_address;
    bool available[translation_port_size];
};

struct nfq_data *packet_buffer[100];
int packet_buffer_count;
pthread_mutex_t packet_buffer_mutex;

struct nat_mapping *translation_table;

struct close_establish{
    int port_number;
    int direction;
};

struct close_establish close_connection[packet_buffer_size];
int close_connection_count = 0;

int token_bucket = 0;
time_t last_generation;

void generate_token_to_bucket(){
    time_t current_time;
    time(&current_time);

    int token_generated = (current_time - last_generation) * fill_rate;

    if (token_bucket + token_generated > bucket_size)
        token_bucket = bucket_size;
    else
        token_bucket += token_generated;

    last_generation = current_time;
}

// Translate LAN IP to NAT IP with assigned port number
struct address_tuple translate_from_in_to_out(struct address_tuple *address){
    int smallest_available_port = 20001;
    for(int i=0; i<translation_port_size; i++){
        // Find any matching entry from previous assignment
        if(strcmp(address->ip_address, translation_table->source_address[i].ip_address) == 0
        && address->port_number == translation_table->source_address[i].port_number){
            return translation_table->translated_address[i];
        }
        // ================================================
        if(translation_table->available[i] == true && smallest_available_port > i)
            smallest_available_port = i;
    }
    return translation_table->translated_address[smallest_available_port];
}
// ====================================================

struct address_tuple reverse_translation(struct address_tuple *address){
    int index = address->port_number - 10000;
    return translation_table->source_address[index];
}

// Check any valid entry to translate IP address to LAN
bool translate_from_out_to_in(struct address_tuple *address){
    int index = address->port_number - 10000;
    if(strcmp(address->ip_address, nat_ip) == 0){
        if(translation_table->available[index] == false)
            return true;
        else
            return false;
    }
    return false;
}
// ====================================================

// Display all valid translated entries
void display_translation_table(){
    printf("     Original Source     |     Translated Source     \n");
    printf("=========================|===========================\n");
    for(int i=0; i<translation_port_size; i++){
        if(translation_table->available[i] == false){
            printf("(%s, %d)       | (%s, %d)\n", 
            translation_table->source_address[i].ip_address, 
            translation_table->source_address[i].port_number,
            translation_table->translated_address[i].ip_address,
            translation_table->translated_address[i].port_number);
        }
    }
    printf("=========================|===========================\n\n");
}
// ====================================

// Push packet to list by callback function
int push_packet_to_list(struct nfq_data *packet){
    pthread_mutex_lock(&packet_buffer_mutex);
    if (packet_buffer_count < packet_buffer_size){
        packet_buffer[packet_buffer_count] = packet;
        packet_buffer_count += 1;
        pthread_mutex_unlock(&packet_buffer_mutex);
        return 1;
    }
    pthread_mutex_unlock(&packet_buffer_mutex);
    return 0;
}
// ========================================

// Dequeue first packet from list
void dequeue_packet_buffer(){
    pthread_mutex_lock(&packet_buffer_mutex);
    for(int i=1; i<packet_buffer_count; i++)
        packet_buffer[i - 1] = packet_buffer[i];
    packet_buffer_count -= 1;
    pthread_mutex_unlock(&packet_buffer_mutex);
}
// ==============================

char* cast_ip_address_to_string(uint32_t raw_ip){
    char *output = malloc(sizeof(char) * 16);
    int a, b, c, d;
    a = raw_ip% 256;
    b = (raw_ip / 256) % 256;
    c = ((raw_ip / 256) / 256) % 256;
    d = raw_ip / (256 * 256 * 256);
    sprintf(output, "%d.%d.%d.%d", a, b, c, d);
    return output;
}

uint32_t cast_string_to_ip_address(char* str){
    char *p;
    p = strtok(str, ".");
    int count = 3;
    uint32_t ip_sum = 0;
    while(p != NULL){
        ip_sum = ip_sum + atoi(p) * pow(256, count);
        count -= 1;
        p = strtok(NULL, ".");
    }
    return ip_sum;
}

void modify_source_address(unsigned char *pktData, struct address_tuple translated_address){
    struct iphdr* ip_header = (struct iphdr*)pktData;
    struct ip* ip_hdr = (struct ip*)pktData;
    struct tcphdr* tcp_header = (struct tcphdr*)((unsigned char*)pktData + (ip_hdr->ip_hl << 2));

    char *source_ip = cast_ip_address_to_string(ip_header->saddr);
    // char *dest_ip = cast_ip_address_to_string(ip_header->daddr);

    struct address_tuple inside_address;
    inside_address.port_number = ntohs(tcp_header->source);
    strcpy(inside_address.ip_address, source_ip);

    // Get translated address and port number
    if(translation_table->available[(int)translated_address.port_number - 10000] == true){
        strcpy(translation_table->source_address[(int)translated_address.port_number - 10000].ip_address, source_ip);
        translation_table->source_address[(int)translated_address.port_number - 10000].port_number = inside_address.port_number;
        translation_table->available[(int)translated_address.port_number - 10000] = false;
        display_translation_table();
    }
    // ======================================

    // Convert source IP address and port number
    ip_header->saddr = htonl(cast_string_to_ip_address(translated_address.ip_address));
    tcp_header->source = htons(translated_address.port_number);
    // =========================================

    // Convert checksum for new modified packet
    tcp_header->check = tcp_checksum(pktData);
    ip_header->check = ip_checksum((unsigned char *) ip_header);
    // ========================================

    // Only for debugging purpose
    // printf("\n=====================================================\n");
    // printf("After translation...\n");
    // source_ip = cast_ip_address_to_string(ip_header->saddr);
    // dest_ip = cast_ip_address_to_string(ip_header->daddr);
    // printf("Source IP Address: %s\n", source_ip);
    // printf("Destination IP Address: %s\n", dest_ip);
    // printf("Source IP Port: %u\n", ntohs(tcp_header->source));
    // printf("Destination IP Port: %u\n", ntohs(tcp_header->dest));
    // show_checksum(pktData, 1);
    // printf("\n=====================================================\n");
    // ===========================
}

void modify_dest_address(unsigned char *pktData, struct address_tuple translated_address){
    struct iphdr* ip_header = (struct iphdr*)pktData;
    struct ip* ip_hdr = (struct ip*)pktData;
    struct tcphdr* tcp_header = (struct tcphdr*)((unsigned char*)pktData + (ip_hdr->ip_hl << 2));

    // char *source_ip = cast_ip_address_to_string(ip_header->saddr);
    char *dest_ip = cast_ip_address_to_string(ip_header->daddr);

    struct address_tuple inside_address;
    inside_address.port_number = ntohs(tcp_header->dest);
    strcpy(inside_address.ip_address, dest_ip);

    // Convert source IP address and port number
    ip_header->daddr = htonl(cast_string_to_ip_address(translated_address.ip_address));
    tcp_header->dest = htons(translated_address.port_number);
    // =========================================

    // Convert checksum for new modified packet
    tcp_header->check = tcp_checksum(pktData);
    ip_header->check = ip_checksum((unsigned char *) ip_header);
    // ========================================

    // Only for debugging purpose
    // printf("\n=====================================================\n");
    // printf("After translation...\n");
    // source_ip = cast_ip_address_to_string(ip_header->saddr);
    // dest_ip = cast_ip_address_to_string(ip_header->daddr);
    // printf("Source IP Address: %s\n", source_ip);
    // printf("Destination IP Address: %s\n", dest_ip);
    // printf("Source IP Port: %u\n", ntohs(tcp_header->source));
    // printf("Destination IP Port: %u\n", ntohs(tcp_header->dest));
    // show_checksum(pktData, 1);
    // printf("\n=====================================================\n");
    // ===========================
}

void fin_count(int port_number, int direction){
    bool found = false;
    for(int i=0; i<close_connection_count; i++){
        if(port_number == close_connection[i].port_number){
            close_connection[i].direction += direction;
            found = true;
            break;
        }
    }

    if(found == false){
        close_connection[close_connection_count].port_number = port_number;
        close_connection[close_connection_count].direction += direction;
        close_connection_count += 1;
    }
}

void find_delete_entry(int port_number){
    bool deleted = false;
    int closed_index = 0;
    for(int i=0; i<close_connection_count; i++){
        if(port_number == close_connection[i].port_number){
            if (close_connection[i].direction == 0){
                int index = port_number - 10000;
                // printf("DELETE INDEX: %d\n", index);
                translation_table->available[index] = true;
                deleted = true;
                closed_index = i;
                display_translation_table();
            }
        }
    }
    if(deleted == true){
        for(int i=closed_index+1; i<close_connection_count; i++){
            close_connection[i - 1].port_number = close_connection[i].port_number;
            close_connection[i - 1].direction = close_connection[i].direction;
        }
        close_connection_count -= 1;
    }
}

void print_delete_list(){
    printf(" Port Number         | Direction\n");
    for(int i=0; i<close_connection_count; i++){
        printf("%d              %d\n", close_connection[i].port_number, close_connection[i].direction);
    }
    printf("====================================\n");
}

void *packet_handler(){
    u_int32_t id = 0;
    struct nfqnl_msg_packet_hdr *header;
    unsigned char *pktData;

    while(1){
        if(packet_buffer_count > 0){
            struct nfq_data *pkt;
            pkt = packet_buffer[0];
            dequeue_packet_buffer();

            // Get packet ID
            header = nfq_get_msg_packet_hdr(pkt);
            if (header != NULL){
                id = ntohl(header->packet_id);
            }
            // =============

            // Put new payload with using data_len
            int data_len = nfq_get_payload(pkt, &pktData);
            // ===============

            // Use casting to manipulate headers
            struct iphdr* ip_header = (struct iphdr*)pktData;
            struct ip* ip_hdr = (struct ip*)pktData;

            if (ip_header->protocol != IPPROTO_TCP){
                nfq_set_verdict(queue_handler, id, NF_DROP, data_len, pktData);
                continue;
            }

            char *source_ip = cast_ip_address_to_string(ip_header->saddr);
            char *dest_ip = cast_ip_address_to_string(ip_header->daddr);
            // printf("Source IP Address: %s\n", source_ip);
            // printf("Destination IP Address: %s\n", dest_ip);

            struct tcphdr* tcp_header = (struct tcphdr*)((unsigned char*)pktData + (ip_hdr->ip_hl << 2));

            // printf("Source IP Port: %u\n", ntohs(tcp_header->source));
            // printf("Destination IP Port: %u\n", ntohs(tcp_header->dest));

            unsigned int local_mask = 0xffffffff << (32 - subnet_mask);

            if (tcp_header->syn == true){
                // printf("\nSyn flags\n");
                // printf("ntohl: %zu, local: %zu\n", ntohl(ip_header->saddr) & local_mask, LAN_number);
                if ((ntohl(ip_header->saddr) & local_mask) == LAN_number){
                    // outbound traffic
                    struct address_tuple inside_address;
                    inside_address.port_number = ntohs(tcp_header->source);
                    strcpy(inside_address.ip_address, source_ip);
                    // printf("OUTBOUND TRAFFIC\n\n");

                    struct address_tuple translated_address = translate_from_in_to_out(&(inside_address));
                    modify_source_address(pktData, translated_address);
                } 
                else {
                    // inbound traffic
                    struct address_tuple inside_address;
                    inside_address.port_number = ntohs(tcp_header->dest);
                    strcpy(inside_address.ip_address, dest_ip);
                    // printf("INBOUND TRAFFIC\n\n");
      
                    if(translate_from_out_to_in(&inside_address)){
                        struct address_tuple translated_address = reverse_translation(&(inside_address));
                        modify_dest_address(pktData, translated_address);
                    }
                    else{
                        // Assume no new flow is initiated from outside network
                        nfq_set_verdict(queue_handler, id, NF_DROP, data_len, pktData);
                        // ====================================================
                        continue;
                    }
                }
            }
            else if(tcp_header->fin == true){
                // printf("\nFin flags\n");

                if ((ntohl(ip_header->saddr) & local_mask) == LAN_number){
                    // outbound traffic
                    // printf("OUTBOUND TRAFFIC\n\n");
                    struct address_tuple inside_address;
                    inside_address.port_number = ntohs(tcp_header->source);
                    strcpy(inside_address.ip_address, source_ip);
                    struct address_tuple translated_address = translate_from_in_to_out(&(inside_address));
                    
                    if(translate_from_out_to_in(&translated_address)){
                        modify_source_address(pktData, translated_address);
                        fin_count(translated_address.port_number, 1);
                    }
                    else{
                        // No matching entry in translation table
                        nfq_set_verdict(queue_handler, id, NF_DROP, data_len, pktData);
                        // ====================================================
                        continue;
                    }
                } 
                else {
                    // inbound traffic
                    // printf("INBOUND TRAFFIC\n\n");
                    struct address_tuple inside_address;
                    inside_address.port_number = ntohs(tcp_header->dest);
                    strcpy(inside_address.ip_address, dest_ip);

                    if(translate_from_out_to_in(&inside_address)){
                        struct address_tuple translated_address = reverse_translation(&(inside_address));
                        modify_dest_address(pktData, translated_address);
                        fin_count(inside_address.port_number, -1);
                    }
                    else{
                        // No matching entry in translation table
                        nfq_set_verdict(queue_handler, id, NF_DROP, data_len, pktData);
                        // ====================================================
                        continue;
                    }
                }        
            }
            else if(tcp_header->rst == true){
                // printf("\nRst flags\n");
                bool deleteEntry = false;
                int deleteIndex;

                if ((ntohl(ip_header->saddr) & local_mask) == LAN_number){
                    // outbound traffic
                    // printf("OUTBOUND TRAFFIC\n\n");
                    struct address_tuple inside_address;
                    inside_address.port_number = ntohs(tcp_header->source);
                    strcpy(inside_address.ip_address, source_ip);
                    struct address_tuple translated_address = translate_from_in_to_out(&(inside_address));
                    
                    if(translate_from_out_to_in(&translated_address)){
                        modify_source_address(pktData, translated_address);
                        deleteIndex = (int)translated_address.port_number - 10000;
                        deleteEntry = true;
                    }
                    else{
                        // No matching entry in translation table
                        nfq_set_verdict(queue_handler, id, NF_DROP, data_len, pktData);
                        // ====================================================
                        continue;
                    }
                } 
                else {
                    // inbound traffic
                    // printf("INBOUND TRAFFIC\n\n");
                    struct address_tuple inside_address;
                    inside_address.port_number = ntohs(tcp_header->dest);
                    strcpy(inside_address.ip_address, dest_ip);

                    if(translate_from_out_to_in(&inside_address)){
                        struct address_tuple translated_address = reverse_translation(&(inside_address));
                        modify_dest_address(pktData, translated_address);
                        deleteIndex = (int)inside_address.port_number - 10000;
                        deleteEntry = true;

                    }
                    else{
                        // No matching entry in translation table
                        nfq_set_verdict(queue_handler, id, NF_DROP, data_len, pktData);
                        // ====================================================
                        continue;
                    }
                }
                // Delete corresponding entry
                if (deleteEntry == true){
                    translation_table->available[deleteIndex] = true;
                    display_translation_table();
                }
                // ==========================
            }
            else if(tcp_header->ack == true){
                // printf("\nAck flags\n");
                // printf("ntohl: %zu, local: %zu\n", ntohl(ip_header->saddr) & local_mask, LAN_number);

                if ((ntohl(ip_header->saddr) & local_mask) == LAN_number){
                    // outbound traffic
                    // printf("OUTBOUND TRAFFIC\n\n");
                    struct address_tuple inside_address;
                    inside_address.port_number = ntohs(tcp_header->source);
                    strcpy(inside_address.ip_address, source_ip);
                    struct address_tuple translated_address = translate_from_in_to_out(&(inside_address));
                    
                    if(translate_from_out_to_in(&translated_address)){
                        modify_source_address(pktData, translated_address);
                        find_delete_entry(translated_address.port_number);
                    }
                    else{
                        // No matching entry in translation table
                        nfq_set_verdict(queue_handler, id, NF_DROP, data_len, pktData);
                        // ====================================================
                        continue;
                    }
                }
                else {
                    // inbound traffic
                    // printf("INBOUND TRAFFIC\n\n");
                    struct address_tuple inside_address;
                    inside_address.port_number = ntohs(tcp_header->dest);
                    strcpy(inside_address.ip_address, dest_ip);

                    if(translate_from_out_to_in(&inside_address)){
                        struct address_tuple translated_address = reverse_translation(&(inside_address));
                        modify_dest_address(pktData, translated_address);
                        find_delete_entry(inside_address.port_number);
                    }
                    else{
                        // No matching entry in translation table
                        nfq_set_verdict(queue_handler, id, NF_DROP, data_len, pktData);
                        // ====================================================
                        continue;
                    }
                }
            }
            
            // Check for bucket and wait for enough token to send
            generate_token_to_bucket();
            if(token_bucket > 0){
                nfq_set_verdict(queue_handler, id, NF_ACCEPT, data_len, pktData);
                token_bucket -= 1;
            }
            else{
                // 1 second = 1,000,000 us
                bool sent = false;
                while(!sent){
                    usleep(1000000 / fill_rate);
                    generate_token_to_bucket();
                    if(token_bucket > 0){
                        nfq_set_verdict(queue_handler, id, NF_ACCEPT, data_len, pktData);
                        token_bucket -= 1;
                        sent = true;
                    }
                }
            }
            // ==================================================
            // print_delete_list();
            // display_translation_table();
            // =================================

            free(source_ip);
            free(dest_ip);
        }
        usleep(100);
    }
    pthread_exit(NULL);
}

static int callback(struct nfq_q_handle *qh, struct nfgenmsg *nfmsg, struct nfq_data *pkt, void *data){
    // Push packet to list handled by thread
	return push_packet_to_list(pkt);
    // =================================
}

// Initialize NFQUEUE related stuffs
void init_handler(){
    // Open a NFQUEUE handler
    nfqueue_handler = nfq_open();
    if (!nfqueue_handler) {
            fprintf(stderr, "Error during nfq_open()\n");
            exit(1);
    }
    // =======================

    // Userspace queueing is handle by NFQUEUE for the selected protocol
    // Need to run in previleged mode
    if (nfq_unbind_pf(nfqueue_handler, AF_INET) < 0) {
            fprintf(stderr, "Error during nfq_unbind_pf()\n");
            exit(1);
    }

    if (nfq_bind_pf(nfqueue_handler, AF_INET) < 0) {
            fprintf(stderr, "Error during nfq_bind_pf()\n");
            exit(1);
    }
    // =================================================================

    // Bind the program to a specific queue
    queue_handler = nfq_create_queue(nfqueue_handler, 0, &callback, NULL);
	if (!queue_handler) {
		fprintf(stderr, "Error during nfq_create_queue()\n");
		exit(1);
	}
    // =====================================

    // Set the amount of packet data that nfqueue copies to userspace
    if (nfq_set_mode(queue_handler, NFQNL_COPY_PACKET, 0xffff) < 0) {
		fprintf(stderr, "can't set packet_copy mode\n");
		exit(1);
	}
    // ==============================================================
    // printf("Finish initialization of NFQUEUE handler\n");
}
// =================================

int main(int argc, char **argv){
    time (&last_generation);
    char buf[4096];
    int res;

    // Get arguments and save as global variables
    if (argc != 6){
        fprintf(stderr, "Without sufficient arguments\n");
        exit(1);
    }
    nat_ip = argv[1];
    local_network = argv[2];
    subnet_mask = atoi(argv[3]);
    bucket_size = atoi(argv[4]);
    fill_rate = atoi(argv[5]);
    LAN_number = cast_string_to_ip_address(local_network);
    // ==========================================

    pthread_t packet_tid;
    pthread_create(&packet_tid, NULL, packet_handler, NULL);

    packet_buffer_count = 0;
    pthread_mutex_init(&packet_buffer_mutex, NULL);

    // Initialize translation table and assume size is (12000 - 10000 + 1)
    translation_table = (struct nat_mapping *) malloc(sizeof(struct nat_mapping));
    translation_table->source_address = malloc(sizeof(struct address_tuple) * translation_port_size);
    translation_table->translated_address = malloc(sizeof(struct address_tuple) * translation_port_size);
    for(int i=0; i<translation_port_size; i++){
        strcpy(translation_table->translated_address[i].ip_address, nat_ip);
        translation_table->translated_address[i].port_number = 10000 + i;
        translation_table->available[i] = true;
    }

    for(int i=0; i<packet_buffer_size; i++){
        close_connection[i].direction = 0;
    }
    // ===================================================================
    init_handler();

    // Get the file descriptor associated with the nfqueue handler
    struct nfnl_handle *netlinkHandle = nfq_nfnlh(nfqueue_handler);
	nfqueue_fd = nfnl_fd(netlinkHandle);
    // ===========================================================

    // Block main thread and never end
	while ((res = recv(nfqueue_fd, buf, sizeof(buf), 0)) && res >= 0){
		nfq_handle_packet(nfqueue_handler, buf, res);
	}
    // ================================

    // Destroy the queue
    nfq_destroy_queue(queue_handler);
    // =================

    // Close NFQUEUE handler
    nfq_close(nfqueue_handler);
    // =====================

    pthread_join(packet_tid, NULL);

    return 0;
}