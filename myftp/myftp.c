#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include <errno.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>

#include "myftp.h"

ftpMessage* list_request(){
    ftpMessage *listRequestMessage = (ftpMessage *) malloc(sizeof(ftpMessage));
    strcpy(listRequestMessage->protocol, "myftp");
    listRequestMessage->type = 0xA1;
    listRequestMessage->length = 11;
    return listRequestMessage;
}

ftpMessage* list_reply(int payloadLength){
    ftpMessage *listReplyMessage = (ftpMessage *) malloc(sizeof(ftpMessage));
    strcpy(listReplyMessage->protocol, "myftp");
    listReplyMessage->type = 0xA2;
    listReplyMessage->length = 11 + payloadLength;
    return listReplyMessage;
}

ftpMessage* get_request(int payloadLength){
    ftpMessage *getRequestMessage = (ftpMessage *) malloc(sizeof(ftpMessage));
    strcpy(getRequestMessage->protocol, "myftp");
    getRequestMessage->type = 0xB1;
    getRequestMessage->length = 11 + payloadLength;
    return getRequestMessage;
}

ftpMessage* get_reply(bool existance){
    ftpMessage *getReplyMessage = (ftpMessage *) malloc(sizeof(ftpMessage));
    if(existance)
        getReplyMessage->type = 0xB2;
    else
        getReplyMessage->type = 0xB3;
    strcpy(getReplyMessage->protocol, "myftp");
    getReplyMessage->length = 11;
    return getReplyMessage;
}

ftpMessage* put_request(int payloadLength){
    ftpMessage *putRequestMessage = (ftpMessage *) malloc(sizeof(ftpMessage));
    strcpy(putRequestMessage->protocol, "myftp");
    putRequestMessage->type = 0xC1;
    putRequestMessage->length = 11 + payloadLength;
    return putRequestMessage;
}

ftpMessage* put_reply(){
    ftpMessage *putReplyMessage = (ftpMessage *) malloc(sizeof(ftpMessage));
    strcpy(putReplyMessage->protocol, "myftp");
    putReplyMessage->type = 0xC2;
    putReplyMessage->length = 11;
    return putReplyMessage;
}

ftpMessage* file_data(int fileSize){
    ftpMessage *fileDataMessage = (ftpMessage *) malloc(sizeof(ftpMessage));
    strcpy(fileDataMessage->protocol, "myftp");
    fileDataMessage->type = 0xFF;
    fileDataMessage->length = 11 + fileSize;
    return fileDataMessage;
}

void sendOutFTPMessage(ftpMessage *dataToSendOut, int socket){
    // printf("BEFORE SEND: %d\n", dataToSendOut->length);
    dataToSendOut->length = htonl(dataToSendOut->length);
	int length = send(socket, (void *) dataToSendOut, sizeof(*dataToSendOut), 0);
    // printf("AFTER SEND: %d\n", dataToSendOut->length);
	free(dataToSendOut);
	if(length < 0){
		printf("Connection Error: %s (Errno:%d)\n", strerror(errno), errno);
	}
}

ftpMessage* receiveFTPMessage(int socket){
    ftpMessage *receiveMessage = (ftpMessage *) malloc(sizeof(ftpMessage));
	int length = recv(socket, (void *) receiveMessage, sizeof(*receiveMessage), 0);
    receiveMessage->length = ntohl(receiveMessage->length);
	if(length < 0){
		printf("Connection Error: %s (Errno:%d)\n", strerror(errno), errno);
	}
    return receiveMessage;
}