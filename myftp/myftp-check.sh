#!/bin/sh

make

./server.o 12345 &
serverPID=$!

./client.o 127.0.0.1 12345 list

fileSize=("1M" "5M" "10M" "50M" "80M" "100M" "200M" "500M" "800M" "1G")

for (( i=1; i <= ${#fileSize[@]}; ++i )); do
    generateFile="testcase"$i".txt"
    # fallocate -l data/$generateFile
    dd if=/dev/urandom of=data/$generateFile bs=$i count=10
    ./client.o 127.0.0.1 12345 get $generateFile
    originalFile=`md5sum data/${generateFile} | awk '{ print $1 }'`
    receivedFile=`md5sum ${generateFile} | awk '{ print $1 }'`

    if [ $originalFile != $receivedFile ]; then
        echo "MD5sum mismatch for ftp get testcase "$i
        exit 1
    else
        echo "MD5sum okay for ftp get testcase "$i
    fi

    rm $generateFile data/$generateFile
done

for (( i=1; i <= ${#fileSize[@]}; ++i )); do
    generateFile="testcase"$i".txt"
    # fallocate -l $generateFile
    dd if=/dev/urandom of=$generateFile bs=$i count=10
    ./client.o 127.0.0.1 12345 put $generateFile
    originalFile=`md5sum ${generateFile} | awk '{ print $1 }'`
    receivedFile=`md5sum data/${generateFile} | awk '{ print $1 }'`

    if [ $originalFile != $receivedFile ]; then
        echo "MD5sum mismatch for ftp put testcase "$i
        exit 1
    else
        echo "MD5sum okay for ftp put testcase "$i
    fi

    rm $generateFile data/$generateFile
done

kill $serverPID