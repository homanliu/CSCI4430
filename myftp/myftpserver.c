#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include <signal.h>
#include <pthread.h>
#include <assert.h>
#include <unistd.h>
#include <errno.h>

#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <dirent.h>

#include "myftp.h"

typedef struct threadInfo{
	struct sockaddr_in clientAddress;
	int client_sd;
} clientData;

void signal_handler(int);
void sendListReply(int);
void messageAction(ftpMessage* , int);
void *connectionFunction(void *);
char* listDirectory();
void socketBinding(struct sockaddr_in, int);
void sendOutFTPMessage(ftpMessage *, int);
void sendGetReply(ftpMessage*, int);
void uploadFileFromClient(ftpMessage*, int);

int portNumber;
bool connection = true;
int threadNumber = 0;
pthread_t *threadID;
// struct message_s received;

void signal_handler(int signal){
	if(signal == SIGINT){
		connection = false;
		for(int i=0; i<threadNumber; i++)
			pthread_join(threadID[i], NULL);
		exit(0);
	}
}

void sendListReply(int clientSocket){
	char *listContent = listDirectory();

	// First send out list_reply structure to notify client
	ftpMessage *listReplyMessage = list_reply(strlen(listContent));
	// send(clientSocket, (void *) listReplyMessage, sizeof(*listReplyMessage), 0);
	sendOutFTPMessage(listReplyMessage,clientSocket);

	int length = send(clientSocket, listContent, strlen(listContent), 0);
	free(listContent);
	if(length < 0){
		printf("Connection Error: %s (Errno:%d)\n", strerror(errno), errno);
	}
}

void sendGetReply(ftpMessage* receivedFromClient, int clientSocket){
	int filenameLength = receivedFromClient->length - 11;
	char filename[filenameLength];
	int receivedLength = recv(clientSocket, filename, filenameLength, 0);
	if(receivedLength < 0)
		printf("Connection Error: %s (Errno:%d)\n", strerror(errno), errno);
	else
		filename[receivedLength] = '\0';
	// printf("File Name: %s\n", filename);

	// File Path Parsing
	char filePath[receivedLength + 2 + 4];
	// Only for Linux/Unix Environment
	strcpy(filePath, "data/");
	strcat(filePath, filename);
	printf("File Path: %s\n", filePath);
	//

	FILE *fp = fopen(filePath, "rb");
	if (ENOENT == errno){
		ftpMessage *getErrorReplyMessage = get_reply(false);
		sendOutFTPMessage(getErrorReplyMessage, clientSocket);
		return;
	}
	fseek(fp, 0, SEEK_END); 	// seek to end of file
	int fileSize = ftell(fp); 	// get current file pointer
	fseek(fp, 0, SEEK_SET); 	// seek back to beginning of file

	ftpMessage *getReplyMessage = get_reply(true);
	sendOutFTPMessage(getReplyMessage, clientSocket);

	ftpMessage *fileDataMessage = file_data(fileSize);
	sendOutFTPMessage(fileDataMessage, clientSocket);

	int wordSize = 1024;
	while(!feof(fp)){
		char words[wordSize];
		size_t bytes_read = fread(words, sizeof(char), wordSize - 1, fp);
		ftpMessage *fileDataMessage = file_data(bytes_read * sizeof(char));
		sendOutFTPMessage(fileDataMessage, clientSocket);
		send(clientSocket, words, bytes_read * sizeof(char), 0);
	}
	printf("File sending is finished\n");
	fclose(fp);
}

void uploadFileFromClient(ftpMessage* receivedFromClient, int clientSocket){
	ftpMessage *putReplyMessage = put_reply();
	sendOutFTPMessage(putReplyMessage, clientSocket);
	char filePath[receivedFromClient->length - 11];
	int length = recv(clientSocket, filePath, receivedFromClient->length - 11, 0);
	filePath[length] = '\0';
	char targetFile[100] = {'\0'};
	printf("File Path: %s\n", filePath);

	strcpy(targetFile, "data/");
	strcat(targetFile, filePath);
	printf("File Path: %s\n", targetFile);
	FILE *fp = fopen(targetFile, "w+b");

	// Receive file data with size
	// ftpMessage fileData;
	// recv(clientSocket, (void *) &fileData, sizeof(fileData), 0);
	ftpMessage* fileData = receiveFTPMessage(clientSocket);


	if(!(strcmp(fileData->protocol, "myftp") == 0 && fileData->type == (int)0xFF)){
		printf("File data structure error!\n");
		exit(0);
	}
	int fileDataLength = fileData->length - 11;
	printf("Waiting file... : %d\n", fileDataLength);

	// wordSize depends on connection, it can be changed
	int wordSize = 1024;
	while(fileDataLength > 0){
		char words[wordSize];

		ftpMessage* fileSizeData = receiveFTPMessage(clientSocket);
		if(fileSizeData->length - 11 > wordSize)
			fileSizeData->length = ntohl(fileSizeData->length);
		printf("%d\n", fileSizeData->length - 11);
		int length = recv(clientSocket, words, fileSizeData->length - 11, 0);
		fileDataLength -= fileSizeData->length - 11;
		fwrite(words, sizeof(char), fileSizeData->length - 11, fp);
	}
	printf("File has been received and saved!!!\n");
	fclose(fp);
}

void messageAction(ftpMessage* receivedFromClient, int clientSocket){
	int messageLength = (int)sizeof(receivedFromClient->protocol) + (int)sizeof(receivedFromClient->type) + (int)sizeof(receivedFromClient->length);
	if(strcmp(receivedFromClient->protocol, "myftp") != 0){
		char *errorMessage = "Wrong protocol to handle";
		printf("%s\n", errorMessage);
		send(clientSocket, errorMessage, strlen(errorMessage), 0);
	}
	else if(receivedFromClient->length != messageLength && (int)receivedFromClient->type == (int)0xA1){
		// Needs to change condition here since put_request and get_request have payload
		char *errorMessage = "Wrong message size";
		printf("%s\n", errorMessage);
		send(clientSocket, errorMessage, strlen(errorMessage), 0);
	}
	else{
		printf("Processing...\n");
		// LIST_REQUEST
		if((int)receivedFromClient->type == (int)0xA1){
			sendListReply(clientSocket);
		}
		// GET_REQUEST
		else if((int)receivedFromClient->type == (int)0xB1){
			sendGetReply(receivedFromClient, clientSocket);
		}
		// PUT_REQUEST
		else if((int)receivedFromClient->type == (int)0xC1){
			uploadFileFromClient(receivedFromClient, clientSocket);
		}
	}
}

void *connectionFunction(void *client_sd){
	char buffer[100];
	int length;
	int clientSocket = *((int *) client_sd);
	// printf("RECEIVING SOMETHING\n");
	ftpMessage* receivedMessage;
	// Client is now connected to server
	// recv(clientSocket, (void *) receivedMessage, sizeof(receivedMessage), 0);
	receivedMessage = receiveFTPMessage(clientSocket);
	printf("Received from sender: %s 0x%02X %d\n", receivedMessage->protocol, receivedMessage->type, receivedMessage->length);
	messageAction(receivedMessage, clientSocket);
	close(clientSocket);
	pthread_exit(NULL);
}

char* listDirectory(){
	struct dirent *directoryEntry;
	char *fileList = (char *) malloc(sizeof(char));
	int stringTotalLength = 0;
	strcpy(fileList, "");
	DIR* dir = opendir("data");
	if (ENOENT == errno){
		mkdir("data", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
		dir = opendir("data");
	}
	while((directoryEntry = readdir(dir)) != NULL){
		if(strcmp(directoryEntry->d_name, ".") == 0 || strcmp(directoryEntry->d_name, "..") == 0)
			continue;

		char *writeIn = (char *) malloc(sizeof(char *) * (strlen(directoryEntry->d_name) + 1 + sizeof('\n')));
		strcpy(writeIn, directoryEntry->d_name);
		strcat(writeIn,"\n");
		stringTotalLength += (strlen(writeIn));
		// printf(" listDirectory() Function: %s",writeIn);
		char *tempString = (char *) realloc(fileList, sizeof(char *) * (stringTotalLength));
		fileList = tempString;
		strcat(fileList, writeIn);
	}
	closedir(dir);
	return fileList;
}

void socketBinding(struct sockaddr_in serverAddress, int socketDescriptor){
	// Create a socket and bind to a port
	memset(&serverAddress, 0, sizeof(serverAddress));
	serverAddress.sin_family = AF_INET;
    // Accept all IP addresses incoming
	serverAddress.sin_addr.s_addr = htonl(INADDR_ANY);
	serverAddress.sin_port = htons(portNumber);
    //

	if (bind(socketDescriptor, (struct sockaddr *) &serverAddress, sizeof(serverAddress)) < 0){
		printf("Bind Error: %s (Errno:%d)\n", strerror(errno), errno);
		exit(0);
	}

	if (listen(socketDescriptor, 3) < 0){
		printf("Listen Error: %s (Errno:%d)\n",strerror(errno), errno);
		exit(0);
	}
	// The above states are prepared for listening any clients
}

int main(int argc, char** argv){
	struct sockaddr_in serverAddress;
	threadID = (pthread_t *) malloc(sizeof(pthread_t));

	// Override default Ctrl+C
	signal(SIGINT, signal_handler);

    // Return error when argc != 2
    assert(argc == 2);
    portNumber = atoi(argv[1]);
    
    int socketDescriptor = socket(AF_INET, SOCK_STREAM, 0);
    if (socketDescriptor == -1){
        printf("Socket descriptor creation error.\n");
        exit(0);
    }

    printf("Server started at port: %d \n",portNumber);

	// Port reusable
	long val = 1;
	if(setsockopt(socketDescriptor, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(long)) == -1){
		perror("setsockopt");
		exit(1);
	}

    socketBinding(serverAddress, socketDescriptor);
	
	while(connection){
		struct sockaddr_in clientAddress;
		int addressLength = sizeof(clientAddress);
		// Accept() will be blocked until client is connected
		int client_sd = accept(socketDescriptor, (struct sockaddr *) &clientAddress, &addressLength);
		if(client_sd < 0){
			printf("Accept Error: %s (Errno:%d)\n",strerror(errno), errno);
			exit(0);
		}
		pthread_create(&threadID[threadNumber], NULL, connectionFunction, &client_sd);
		threadNumber += 1;
		pthread_t *newPointer = (pthread_t *) realloc(threadID, sizeof(pthread_t) * (threadNumber + 1));
		if(newPointer != NULL)
			threadID = newPointer;
	}

	for(int i=0; i<threadNumber; i++)
		pthread_join(threadID[i], NULL);
	
	free(threadID);
	close(socketDescriptor);
    return 0;
}
