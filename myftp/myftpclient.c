#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include <signal.h>
#include <pthread.h>
#include <assert.h>
#include <unistd.h>

#include <errno.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "myftp.h"

int portNumber, socketDescriptor;
char IPaddress[15];
bool connection = true;

int main(int argc, char** argv){
    int littleEndian = 0x12345678;

    // Return error when argc < 3 || argc > 5
    assert(argc >= 3 && argc <= 5);
    portNumber = atoi(argv[2]);
    strcpy(IPaddress, argv[1]);

    socketDescriptor = socket(AF_INET, SOCK_STREAM, 0);
    if (socketDescriptor == -1){
        printf("Socket descriptor creation error.\n");
        exit(0);
    }

	// Create a socket and bind to a port
	struct sockaddr_in server_addr;
	memset(&server_addr, 0, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = inet_addr(IPaddress);
	server_addr.sin_port = htons(portNumber);
	if(connect(socketDescriptor, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0){
		printf("Connection Error: %s (Errno:%d)\n", strerror(errno), errno);
		exit(0);
	}
    //

	littleEndian = htonl(littleEndian);

	// Send list struct to server
	if(strcmp(argv[3], "list") == 0){
		ftpMessage *listRequestMessage = list_request();
		sendOutFTPMessage(listRequestMessage,socketDescriptor);
		// send(socketDescriptor, (void *) listRequestMessage, sizeof(*listRequestMessage), 0);
		
		// recv(socketDescriptor, (void *) replyMessage, sizeof(replyMessage), 0);
		ftpMessage* replyMessage = receiveFTPMessage(socketDescriptor);
		if(strcmp(replyMessage->protocol, "myftp") == 0 && replyMessage->type == 162){
			char replyReceived[replyMessage->length - 11];
			int length = recv(socketDescriptor, replyReceived, replyMessage->length - 11, 0);
			if(length < 0)
				printf("Connection Error: %s (Errno:%d)\n", strerror(errno), errno);
			else{
				replyReceived[length] = '\0';
				printf("%s", replyReceived);
			}
		}
	}
	else if(strcmp(argv[3], "get") == 0){
		// Abort if not provided filename
		assert(argc == 5);
		ftpMessage *getRequestMessage = get_request(strlen(argv[4]));
		

		// Send get request
		sendOutFTPMessage(getRequestMessage,socketDescriptor);
		// send(socketDescriptor, (void *) getRequestMessage, sizeof(*getRequestMessage), 0);

		// free(getRequestMessage);
		// Send filename
		send(socketDescriptor, argv[4], strlen(argv[4]), 0);
		ftpMessage* replyMessage = receiveFTPMessage(socketDescriptor);
		// recv(socketDescriptor, (void *) &replyMessage, sizeof(replyMessage), 0);

		// File does not exist replied from server
		if(strcmp(replyMessage->protocol, "myftp") == 0 && replyMessage->type == (int)0xB3){
			printf("File does not exist!\n");
			exit(0);
		}
		// File exists replied from server and going to receive message structure and file data
		else if(strcmp(replyMessage->protocol, "myftp") == 0 && replyMessage->type == (int)0xB2){
			// Receive file data with size
			// ftpMessage fileData;
			// ftpMessage* replyMessage = receiveFTPMessage(socketDescriptor);

			// recv(socketDescriptor, (void *) &fileData, sizeof(fileData), 0);
			ftpMessage* fileData = receiveFTPMessage(socketDescriptor);

			// printf("FILE PROTOCOL: %s", fileData->protocol);
			// printf("FILE TYPE: %d", fileData->type);
			if(!(strcmp(fileData->protocol, "myftp") == 0 && fileData->type == (int)0xFF)){
				printf("File data structure error!\n");
				exit(0);
			}
			int fileDataLength = fileData->length - 11;
			printf("Waiting file... : %d\n", fileDataLength);

			// wordSize depends on connection, it can be changed
			int wordSize = 1024;
			FILE *fp = fopen(argv[4], "w+b");
			while(fileDataLength > 0){
				ftpMessage* fileSizeData = receiveFTPMessage(socketDescriptor);
				char words[wordSize];
				if(fileSizeData->length - 11 > wordSize)
					fileSizeData->length = ntohl(fileSizeData->length);
				printf("%d\n", fileSizeData->length - 11);
				int length = recv(socketDescriptor, words, fileSizeData->length - 11, 0);
				fileDataLength -= fileSizeData->length - 11;
				fwrite(words, sizeof(char), fileSizeData->length - 11, fp);
			}
			printf("File has been received and saved!!!\n");
			fclose(fp);
		}

	}
	else if(strcmp(argv[3], "put") == 0){
		// Abort if not provided filename
		assert(argc == 5);
		FILE *fp = fopen(argv[4], "rb");
		if (!fp){
			printf("File cannot open\n");
			return 0;
		}
		fseek(fp, 0, SEEK_END); 	// seek to end of file
		int fileSize = ftell(fp); 	// get current file pointer
		fseek(fp, 0, SEEK_SET); 	// seek back to beginning of file

		// First send out a put request to server
		printf("String Length: %d\n", (int)strlen(argv[4]));
		ftpMessage *putRequestMessage = put_request(strlen(argv[4]));
		sendOutFTPMessage(putRequestMessage, socketDescriptor);
		send(socketDescriptor, argv[4], strlen(argv[4]), 0);

		// Then wait for put reply from server
		ftpMessage replyMessage;
		recv(socketDescriptor, (void *) &replyMessage, sizeof(replyMessage), 0);
		if(!(strcmp(replyMessage.protocol, "myftp") == 0 && replyMessage.type == (int)0xC2)){
			printf("File data structure error!\n");
			exit(0);
		}

		ftpMessage *fileDataMessage = file_data(fileSize);
		sendOutFTPMessage(fileDataMessage, socketDescriptor);
		int wordSize = 1024;
		while(!feof(fp)){
			char words[wordSize];
			size_t bytes_read = fread(words, sizeof(char), wordSize - 1, fp);
			ftpMessage *fileDataMessage = file_data(bytes_read * sizeof(char));
			sendOutFTPMessage(fileDataMessage, socketDescriptor);
			send(socketDescriptor, words, bytes_read * sizeof(char), 0);
		}
		printf("File sending is finished\n");
		fclose(fp);
	}

	close(socketDescriptor);
	return 0;
}
